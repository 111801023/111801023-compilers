structure TC =
struct

structure p = Tiger;
val out = ref 0
structure prettyprint = PP;
structure printast = PrintAST; 
structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure TigerLex    = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerParser = Join( structure ParserData = TigerLrVals.ParserData
							  structure Lex        = TigerLex
							  structure LrParser   = LrParser
							 )

fun makeTigerLexer strm = TigerParser.makeLexer (fn n => TextIO.inputN(strm,n))
val makeFileLexer      = makeTigerLexer o TextIO.openIn


(* Parse command line and set a suitable lexer *)

val thisLexer = case CommandLine.arguments() of
		    []     => makeTigerLexer TextIO.stdIn
		 | ["--pp", x]  =>  ( out := 0 ; makeFileLexer x)
		 | ["--ast", x]  => ( out := 1 ; makeFileLexer x)
		 |  [x]    => makeFileLexer x
		 |  _      => (TextIO.output(TextIO.stdErr, "usage: tc [arg] file"); OS.Process.exit OS.Process.failure)


fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr,
					    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")

val (program,_) = TigerParser.parse (0,thisLexer,print_error,()) 

val _ = case !out of 
		   0 => prettyprint.print(TextIO.stdOut,program)
		 | 1 => printast.print(TextIO.stdOut,program)
		 | _ => printast.print(TextIO.stdOut,program)

end
