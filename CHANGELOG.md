# **Changelog**
## **Week 0**
Lab Date - 17th February, 2021 -- Initialized a new git repository and added the README and CHANGELOG files.  
Date - 24th February, 2021 -- Uploaded assignment for lab-0 for Hello World program.  
## **Week 1**
Date - 1st March, 2021 -- Uploaded the directory for the Revese Polish assignment and made the required changes for including 'Div' operator along with parantheses.  
## **Week 2**
Date - 9th March, 2021 -- Added directory for tiger language. Created ast.sml file for tiger and mips.sml file. Updated the Makefile to compile the aforementioned SML files as well. Finished the assignment for this week. 

## **Week 3**
Date - 16th March, 2021 -- Having some conceptual difficulties with the assignment. Would make changes by next week.

## ** Week 7**
Date - 29th April, 2021 -- I have now been able to complete my assignment for pretty printing and now the AST and grammar is also working.

##
Date - 2nd May, 2021 -- I have received grade 0 in the evaluation done on Sunday, 2nd May,2021. I request you to please check my submission again and check if there has been any mistake on my end. Thanks

## 
Date - 14th May, 2021 -- I have finished the assignment and have provided the output for each of the labs - 3 and 4 in the folders lab3_output and lab4_output. 

To execute the program:
Firstly, compile the program using "make tiger".  
Then for:
lab3: ./tc --ast tests/test1.tig
lab4: ./tc --pp tests/test1.tig

Hash code: 320fe2d7448a3168b99db55d48d9b3a920bdd93b
Time: 14th May, 2021, 09:34 PM  

