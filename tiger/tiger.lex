type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue, pos) token

val lineRef : pos ref = ref 1;
val linePos : pos list ref = ref [1]

fun inc x = (x := !x +1; !x)
fun dec x = (x := !x -1; !x)

fun updateLine n = lineRef := !(lineRef) + n

val stringBuf : string ref = ref ""
val stringBegin = ref 0

val inString = ref 0;

fun asciiCode str =
    let val subStr = String.substring(str, 1, 3)
        val intVal = valOf(Int.fromString(subStr))
        val charVal = chr intVal
    in Char.toString charVal end

val error : string * int * int -> unit = 
    fn (e, l1, l2) => TextIO.output(TextIO.stdOut, "lex: line "
                        ^ Int.toString l1 ^ " l2= "
                        ^ Int.toString l2
                        ^ ": " ^ e ^ "\n")

val badCh : int * char -> unit = 
    fn (l1, ch) => TextIO.output(TextIO.stdOut, "lex: line "
                        ^ Int.toString l1 ^": Invalid Character"
                        ^ Int.toString (ord ch)^"="^str(ch)^"\n")

val comDepth = ref 0;

(* fun eof() = if !comDepth = 0 then ()
          else TextIO.output(TextIO.stdOut, "Unclosed comment\n");
          Tokens.EOF(!lineRef, !lineRef) *)

exception StringException of string;
exception UnclosedComment;
exception UnclosedString;

fun eof() = let val pos = hd(!linePos) in
              if (!comDepth > 0) then raise UnclosedComment
              else if (!inString = 1) then raise UnclosedString
              else ();
              Tokens.EOF(pos, pos) end

fun withoutQuotes(s:string):string = String.extract (s, 1, SOME(String.size s - 2))

fun charsToInt m (x :: xs) = charsToInt (10 * m + ord x - ord #"0") xs
  | charsToInt m []        = m

fun toSigned (#"-" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"~" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"+" :: xs) =   charsToInt 0 xs
  | toSigned xs           =   charsToInt 0 xs

val toInt        = toSigned o String.explode
val newlineCount = List.length o List.filter (fn x => x = #"\n") o String.explode 



%%

%header (functor TigerLexFun (structure Tokens: Tiger_TOKENS));

ws = [\ \t];
digits = [0-9];
newline = [\n];
quote = ["];
non = [^"];
symbol = [a-zA-Z][0-9a-zA-Z_]*;

%s COMMENT STRING;

%%
<INITIAL>{ws}+            => (lex());
<INITIAL>{newline}        => (inc lineRef; linePos := yypos :: !linePos; lex());

<INITIAL>"array"          => (Tokens.ARRAY (yypos, yypos + 5));
<INITIAL>"if"             => (Tokens.IF (yypos, yypos + 2));
<INITIAL>"then"           => (Tokens.THEN (yypos, yypos + 4));
<INITIAL>"else"           => (Tokens.ELSE (yypos, yypos + 4));
<INITIAL>"while"          => (Tokens.WHILE (yypos, yypos + 5));
<INITIAL>"for"            => (Tokens.FOR (yypos, yypos + 3));
<INITIAL>"to"             => (Tokens.TO (yypos, yypos + 2));
<INITIAL>"do"             => (Tokens.DO (yypos, yypos + 2));
<INITIAL>"let"            => (Tokens.LET (yypos, yypos + 3));
<INITIAL>"in"             => (Tokens.IN (yypos, yypos + 2));
<INITIAL>"end"            => (Tokens.END (yypos, yypos + 3));
<INITIAL>"of"             => (Tokens.OF (yypos, yypos + 2));
<INITIAL>"break"          => (Tokens.BREAK (yypos, yypos + 5));
<INITIAL>"nil"            => (Tokens.NIL (yypos, yypos + 3));
<INITIAL>"function"       => (Tokens.FUNCTION (yypos, yypos + 8));
<INITIAL>"var"            => (Tokens.VAR (yypos, yypos + 3));
<INITIAL>"type"           => (Tokens.TYPE (yypos, yypos + 4));

<INITIAL>","              => (Tokens.COMMA (yypos, yypos + 1));
<INITIAL>":"              => (Tokens.COLON (yypos, yypos +1));
<INITIAL>";"              => (Tokens.SEMICOLON (yypos, yypos + 1));
<INITIAL>"("              => (Tokens.LPAREN (yypos,yypos + 1));
<INITIAL>")"              => (Tokens.RPAREN (yypos, yypos +1));
<INITIAL>"["              => (Tokens.LBRACK (yypos,yypos + 1));
<INITIAL>"]"              => (Tokens.RBRACK (yypos, yypos +1));
<INITIAL>"{"              => (Tokens.LBRACE (yypos,yypos + 1));
<INITIAL>"}"              => (Tokens.RBRACE (yypos, yypos +1));
<INITIAL>"."              => (Tokens.DOT (yypos, yypos +1));
<INITIAL>"+"              => (Tokens.PLUS (yypos, yypos +1));
<INITIAL>"-"              => (Tokens.MINUS (yypos, yypos +1));
<INITIAL>"*"              => (Tokens.TIMES (yypos, yypos +1));
<INITIAL>"/"              => (Tokens.DIVIDE (yypos, yypos +1));
<INITIAL>"="              => (Tokens.EQ (yypos, yypos + 1));
<INITIAL>"<>"             => (Tokens.NEQ (yypos, yypos + 2)); 
<INITIAL>"<"              => (Tokens.LT (yypos, yypos + 1));
<INITIAL>"<="             => (Tokens.LE (yypos, yypos + 2));
<INITIAL>">"              => (Tokens.GT (yypos, yypos + 1));  
<INITIAL>">="             => (Tokens.GE (yypos, yypos + 2));
<INITIAL>":="             => (Tokens.ASSIGN (yypos, yypos + 2));
<INITIAL>"&"              => (Tokens.AND (yypos, yypos + 1));
<INITIAL>"|"              => (Tokens.OR (yypos, yypos +1));

<INITIAL>{digits}+        => (Tokens.INT(valOf(Int.fromString(yytext)), yypos, yypos + size yytext));
<INITIAL>{symbol}             => (Tokens.symbol(yytext, yypos, yypos + size yytext));

<INITIAL>"/*"             => (inc comDepth; YYBEGIN COMMENT; lex());
<INITIAL>"*/"             => (error ("Comment never started\n", !lineRef, hd(!linePos)); lex());

<INITIAL>{quote}{non}*{quote} => (Tokens.STRING( withoutQuotes(yytext),yypos, yypos + size(yytext)));

<COMMENT>"/*"             => (inc comDepth; lex());
<COMMENT>"*/"             => (dec comDepth; if (!comDepth = 0) then YYBEGIN INITIAL else (); lex() );
<COMMENT>{newline}        => (inc lineRef; linePos := yypos :: !linePos; lex());
<COMMENT>{ws}+            => (lex());
<COMMENT>.                => (lex());

<INITIAL>.                => (error ("Unknown Error\n", !lineRef, hd(!linePos)); lex() );
