structure Tiger = struct

type symbol = string;
type typeField = {name:symbol, typ:symbol}

datatype lvalue = SimpleVar of symbol
            | FieldVar of lvalue * symbol
            | SubscriptVar of lvalue * exp

and Prog = Exp of exps
		  | Dec of decs             

and exp = NilExpr
		| VarExpr of lvalue
        | IntegerExpr of int
        | StringExpr of string
        | BreakExpr 
        
        | SeqExpr of exp list
        
        | RecordExpr of {fields: (symbol * exp) list, typ: symbol}
        | ArrayExpr of {typ: symbol, size: exp, init: exp}

        | CallFunctionExpr of {func: symbol, args: exp list}
        
        | AssignExpr of {var: lvalue, exp: exp}
        
        | OperatorExpr of {left: exp, oper: oper, right: exp}
        | NegativeOpExpr of exp
      
        | IfExpr of {test: exp, then': exp, else': exp option}
        | WhileExpr of {test: exp, body: exp}
		| ForExpr of {var: symbol, lo: exp, hi: exp, body: exp}
        | LetExpr of {decs: dec list, body: exp}
        
and exps = Exps of exp list
        
and ty = NameTy of symbol
       | RecordTy of typeField list
       | ArrayTy of symbol
       
and dec = FunctionDec of {name: symbol, params: typeField list, result: symbol option, body: exp} list
        | VarDec of {name: symbol, typ: symbol option, init: exp}
        | TypeDec of {name: symbol, typ: ty} list
        
and decs = Decs of dec list
        
and oper = PlusOp | MinusOp | MultOp | DivideOp | AndOp | OrOp
         | EqOp | NeqOp | LtOp | LeOp | GtOp | GeOp 
      
end (* structure Tiger *)
