structure PrintAST 
	(* sig val print : TextIO.outstream * Tiger.exp -> unit end*) =
struct

structure A = Tiger

fun print (outstream, prog) =     (* A has a datatype 'PROG' which stores program in form of either exps list or decs list *)

let 
	(* outstream set to be "TextIO.stdOut" *)
    fun print s =  TextIO.output(outstream,s) 	(* function tao print to stream*)
    and printnewln s = (print s ; print "\n") 
	
	(* function to iterate over a list and execute a function *)
	and dolist d f [a] = (print ""; f(a,d))
		  | dolist d f (a::r) = (print ""; f(a,d); print ", "; dolist d f r)
		  | dolist d f nil = ()		
		  
	and doseq d f [a] = (f(a,d))
		  | doseq d f (a::r) = (f(a,d); printnewln ";"; doseq d f r)
		  | doseq d f nil = () 
		  
	and dodecs d f [a] = (print ""; f(a,d))
		  | dodecs d f (a::r) = (f(a,d); print "\n"; dodecs d f r)
		  | dodecs d f nil = ()      

	(* function to indent spaces *)
    and indent 0 = ()
      	| indent i = (print " " ; indent(i-1))
		     

	(* function to store operations name *)
    fun opname A.PlusOp = "+"
		| opname A.MinusOp = "-"
		| opname A.MultOp = "*"
		| opname A.DivideOp = "/"
		| opname A.EqOp = "="
		| opname A.NeqOp = "!="
		| opname A.LtOp = "<"
		| opname A.LeOp = "<="
		| opname A.GtOp = ">"
		| opname A.GeOp = ">="
		| opname A.AndOp = "&"
		| opname A.OrOp = "|"


	(* function to handle variables printing *)
    and lvalue (A.SimpleVar(s),d) = (indent d; print "SimpleVar("; print s; print ")" )
		| lvalue (A.FieldVar(v,s),d) = (indent d; print "FieldVar(" ; lvalue(v,d+1); printnewln ","; indent (d+1) ; print s ; print ")")
		| lvalue (A.SubscriptVar(v,e),d) = (indent d; print "SubscriptVar(" ; lvalue(v,d+1); printnewln ","; PrintExprPretty(e,d+1) ; print ")")
		
	(* function to handle type printing *)
    and printtype (A.NameTy(t), d) = (indent d; print "NameTy(" ; print(t); print(")"))
		  | printtype (A.ArrayTy(t),d) = (indent d; print "ArrayTy("; print t ; print ")")
		  | printtype (A.RecordTy(ty_list),d) =   let fun print_tyfield(({name,typ}),d) = (indent d ; print "(" ; print name ; print "," ; print typ ; print ")") 
												  in  (indent d; print "RecordTy[" ; dolist 0 print_tyfield ty_list ; print "]")
												  end

    (* function to handle the printing of declarations expressions  *) 
    and printdec (A.VarDec{name, typ, init}, d) = (indent d; print "VarDec("; print name; print ","; 
    												case typ of NONE => (print "NONE")
    												| SOME(s) => (print "SOME(" ; print s; print ")"); 
    												printnewln ","; PrintExprPretty(init,0) ; print ")")
		  
		  | printdec (A.TypeDec x, d) = let fun f({name, typ}, d) = (indent d; print "(" ; print name ; printnewln "," ; printtype(typ, 0) ; print ")")
		  										in 
		  											(indent d; print "TypeDec[ "; dodecs 0 f x ; print "]")
		  										end
		  | printdec (A.FunctionDec x, d) =  let fun print_tyfield({name,typ},d) = (indent d; print "(" ; print name ; print "," ; print typ ; print ")")
			  									 fun f({name, params, result, body}, d) = ( indent d; print "(" ; print name ; print ",[" ; dolist d print_tyfield params; printnewln "],"; 
						  																   	case result of NONE => (print "NONE")
						  																   		| SOME(s) => (print "SOME(" ; print s ; print ")");
						  																   	printnewln ","; PrintExprPretty(body, d+1) ; print ")" ) 
										     in 
										   	   	(indent d; print "FunctionDec["; dolist d f x; print "]")
										     end
		 (*
		  | printdec (A.PrimitiveDec{name, params, typ}, d)  =    let fun print_tyfield(A.typeField({name,typ}),d) = (print name ; print " : " ; printGreen typ) 
																  in (indent d; printCyan "primitive "; print name ; print "(";
																  dolist 0 print_tyfield params ; print ")";
																  case typ of NONE => print ""
																			 | SOME(s) => (print " : " ; printGreen s)) 
											 			          end
		*)											 			          
	(* function to handle printing of arguments of functions *)                 			          
	(*and printArgs (a::b::c,d) = (PrintExprPretty(a,d) ; print "," ; printArgs(b::c,d))
		| printArgs (a::[],d) = (PrintExprPretty(a,d)) 					    
        | printArgs ([],d) = ()
	*)
    (* function to handle pretty printing of expressions *)
    and PrintExprPretty (A.NilExpr,d) = (indent d ; print "NilExpr")
		 |  PrintExprPretty (A.VarExpr(var), d) = (indent d ; printnewln "VarExpr(" ; lvalue(var, d); print ")")
		 |  PrintExprPretty (A.IntegerExpr(I),d) = (indent d ; print "IntExpr(" ; print (Int.toString(I)); print ")")
		 |  PrintExprPretty (A.StringExpr(st),d) = (indent d ; print "StringExpr(\"" ; print st ; print "\")" )

		 |  PrintExprPretty (A.CallFunctionExpr{func,args},d) = (indent d ;  print "CallExpr(" ; print func ; print ",[" ; dolist d PrintExprPretty args ; print "]")

		 |  PrintExprPretty (A.OperatorExpr{left,oper,right},d) = (indent d;   print "OpExpr(" ; print(opname oper) ; PrintExprPretty(left,0)  ; print ","  ; PrintExprPretty(right,0); print ")")
		 
		 | PrintExprPretty (A.SeqExpr l, d) = (indent d ; print "SeqExpr[" ; doseq d PrintExprPretty l ; print "]")

		 | PrintExprPretty (A.AssignExpr{var,exp},d) = (indent d ; printnewln "AssignExpr(" ; lvalue(var,0) ; printnewln "," ; PrintExprPretty(exp,0) ; print ")")
		 
		 | PrintExprPretty (A.NegativeOpExpr e, d) = ( indent d ; print "NegativeOpExpr(" ; PrintExprPretty(e,d) ; print ")")
	(*	 | PrintExprPretty (A.BracketOpExpr(exps),d) = (indent d ; printnewln "(" ; PrintExprPretty(A.Exp(exp),d + 1) ; printnewln ")")
*)
		 | PrintExprPretty (A.IfExpr{test,then', else'},d) = (indent d; printnewln "IfExpr(" ;  PrintExprPretty(test,0) ; printnewln "," ;
		 																	PrintExprPretty(then',d+1);
		 																	case else' of NONE => ()
		 																				| SOME e => (printnewln "," ; PrintExprPretty(e, d+1); print ")")
		 																	)   
		 | PrintExprPretty (A.WhileExpr{test,body},d) = (indent d; printnewln "WhileExpr(" ; 	PrintExprPretty(test,0) ; printnewln "," ; PrintExprPretty(body,d+1); print ")")
		 
		 | PrintExprPretty (A.ForExpr{var,lo,hi,body},d) =  (indent d; printnewln "ForExpr(" ; print var ;  printnewln "," ; PrintExprPretty(lo,0)
										; print "," ; PrintExprPretty(hi,0) ; printnewln "," ;  PrintExprPretty (body,d+1) ; print ")")
										
		 | PrintExprPretty (A.BreakExpr,d) = (indent d; print "BreakExpr")
		 
		 | PrintExprPretty (A.LetExpr{decs,body},d) = (indent d ; print "LetExpr([" ; dodecs (d+1) printdec decs ; printnewln "]" ; 
								  						PrintExprPretty(body, d+1); print ")")
		
		  | PrintExprPretty ((A.ArrayExpr{typ,size,init},d)) = (indent d; print "ArrayExpr("; print typ; printnewln "," ; PrintExprPretty(size,0) ; printnewln "," ; PrintExprPretty(init,0); print ")")
		 | PrintExprPretty (A.RecordExpr{fields, typ}, d) = let
		 														fun f((name,e),d) = (indent d; print "(" ; print(name); printnewln ","; PrintExprPretty(e,d); print ")")
		 													in
		 														(indent d; print "RecordExpr(" ;  print typ ; printnewln ",[" ; dolist d f fields; print "]")
		 													end					
				

	(* function to handle exps and decs lists *)
	(*and PrintProgramPretty (A.Exp(A.Exps(a::b::c)),d) = (PrintExprPretty(a,d) ; printnewln ";" ; PrintProgramPretty(A.Exp(A.Exps(b::c)),d))
		  | PrintProgramPretty (A.Dec(A.Decs(a::b::c)),d) = (printdec(a,d) ; printnewln "" ; PrintProgramPretty(A.Dec(A.Decs(b::c)),d))		
		  | PrintProgramPretty (A.Exp(A.Exps(a::[])),d) = (PrintExprPretty(a,d) ; printnewln "")
		  | PrintProgramPretty (A.Dec(A.Decs(a::[])),d) = (printdec(a,d) ; printnewln "")						      
		  | PrintProgramPretty (_,d) = ()
	*)
in
	(* main handler function *)
    PrintExprPretty (prog,0); printnewln ""; TextIO.flushOut outstream  
end
end
