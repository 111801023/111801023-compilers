structure MIPS = struct

datatype regs = zero | at | v0 | v1 | a0 | a1 | a2 | a3 | t0 | t1 | t2 | t3 | t4 | t5 | t6 | t7 | t8 | t9 | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 | k0 | k1 | gp | sp | fp | ra;

datatype  ('l,'t) inst = 
						(* arithmetic instructions*)
						add of 't * 't * 't
                        | addi of 't * 't * 'l
                        | addu of 't * 't * 't
                        | sub of 't * 't * 't
                        | mul of 't * 't * 't
                        | div of 't * 't
                        
                        (* Logical instructions *)
                        | xor  of 't * 't * 't 
                      	| xori of 't * 't * 't
                        
                        (* Data movement instruction *) 
                        | move of 't * 't
                        | beq of 't * 't * 'l
                        | blt of 't * 't * 'l
                        | bgt of 't * 't * 'l
                        | bge of 't * 't * 'l
                        | bne of 't * 't * 'l
                        
                        (* load and store instructions *)
                        | la of 't * 'l
		                | lb of 't * 'l
		                | lbu of 't * 'l
		                | ld of 't * 'l
		                | sb of 't * 'l
                        | sd of 't * 'l
                        | sh of 't * 'l
                        | sw of 't * 'l
                        | swcz of 't * 'l
                        | swl of 't * 'l
                        | swr of 't * 'l
                        
                        
                        (* Jump instructions *)
                        | jump of 'l
                        | jal of 'l
                        | jalr of 't
                        | jr of 't
                        
                        (*Exception and Trap Instructions*)
		      			| rfe
                      	| syscall	
                      	| break 
                      	| nop
                      	
                      	(*Comparison Instructions*)
						| seq of 't * 't * 't
						| sge of 't * 't * 't
			            | sgeu of 't * 't * 't	
			            | sgt of 't * 't * 't	
			            | sgtu of 't * 't * 't
			            | sle of 't * 't * 't	
			            | sleu of 't * 't * 't
			            | slt of 't * 't * 't	
end
