structure PP = struct

structure A = Tiger

fun print (outstream, prog) =     (* A has a datatype 'PROG' which stores program in form of either exps list or decs list *)

let 
	(* outstream set to be "TextIO.stdOut" *)
    fun printtostream s =  TextIO.output(outstream,s) 	(* function to print to stream*)
    and printnewln s = (printtostream s ; printtostream "\n") 
	
	fun printRed s =  printtostream ("\027[" ^ "31" ^ "m" ^ s ^ "\027[0m")
    fun printCyan s = printtostream ("\027[" ^ "36" ^ "m" ^ s ^ "\027[0m")
    fun printYellow s =  printtostream ("\027[" ^ "33" ^ "m" ^ s ^ "\027[0m")
    fun printGreen s =  printtostream ("\027[" ^ "32" ^ "m" ^ s ^ "\027[0m")	
    fun printBlue s =  printtostream ("\027[" ^ "34" ^ "m" ^ s ^ "\027[0m")
    fun printPink s =  printtostream ("\027[" ^ "35" ^ "m" ^ s ^ "\027[0m")	

	(* function to iterate over a list and execute a function *)
	and dolist d f [a] = (printtostream ""; f(a,d))
		  | dolist d f (a::r) = (printtostream ""; f(a,d); printtostream ", "; dolist d f r)
		  | dolist d f nil = ()		
		  
	and doseq d f [a] = (f(a,d))
		  | doseq d f (a::r) = (f(a,d); printnewln ";"; doseq d f r)
		  | doseq d f nil = () 
		  
	and dodecs d f [a] = (printtostream ""; f(a,d))
		  | dodecs d f (a::r) = (f(a,d); printtostream "\n"; dodecs d f r)
		  | dodecs d f nil = ()
		  
	and docall d f [a] = (printtostream ""; f(a,d))
		  | docall d f (a::r) = (f(a,d); printtostream ", "; docall d f r)
		  | docall d f nil = ()            

	(* function to indent spaces *)
    and indent 0 = ()
      	| indent i = (printtostream " " ; indent(i-1))
		     

	(* function to store operations name *)
    fun opname A.PlusOp = "+"
		| opname A.MinusOp = "-"
		| opname A.MultOp = "*"
		| opname A.DivideOp = "/"
		| opname A.EqOp = "="
		| opname A.NeqOp = "!="
		| opname A.LtOp = "<"
		| opname A.LeOp = "<="
		| opname A.GtOp = ">"
		| opname A.GeOp = ">="
		| opname A.AndOp = "&"
		| opname A.OrOp = "|"


	(* function to handle variables printing *)
    and lvalue (A.SimpleVar(s),d) = (indent d; printBlue s)
		| lvalue (A.FieldVar(v,s),d) = (indent d; lvalue(v,d); printtostream "."; printBlue s)
		| lvalue (A.SubscriptVar(v,e),d) = (indent d; lvalue(v,d); printtostream "["; PrintExprPretty(e,d); printtostream "]")
		
	(* function to handle type printing *)
    and printtype (A.NameTy(t), d) = (indent d; printGreen t)
		  | printtype (A.ArrayTy(t),d) = (indent d; printYellow "array of "; printGreen t)
		  | printtype (A.RecordTy(ty_list),d) =   let fun print_tyfield(({name,typ}),d) = (printtostream name ; printtostream " : " ; printGreen typ; printtostream "") 
												  in  (indent d; printtostream "{" ; dolist 0 print_tyfield ty_list ; printtostream "}")
												  end

    (* function to handle the printing of declarations expressions  *) 
    and printdec (A.VarDec{name, typ, init}, d) = (indent d; printPink "var "; printBlue name; printtostream " "; 
    												case typ of NONE => ()
    												| SOME(s) => (printtostream ": " ; printYellow s; printtostream " "); 
    												printtostream " := "; PrintExprPretty(init,0))
		  
		  | printdec (A.TypeDec x, d) = let fun f({name, typ}, d) = (printBlue name ; printtostream " = " ; printtype(typ, 0))
		  										in 
		  											(indent d; printPink "type "; dodecs 0 f x)
		  										end
		  | printdec (A.FunctionDec x, d) =  let fun print_tyfield({name,typ},d) = (printGreen name ; printtostream " : " ; printPink typ)
			  									 fun f({name, params, result, body}, d) = (printCyan name ; printtostream " (" ; dolist d print_tyfield params; printtostream ")"; 
						  																   	case result of NONE => ()
						  																   		| SOME(s) => (printtostream " :"; printYellow s);
						  																   	printnewln "="; PrintExprPretty(body, d+1)) 
										     in 
										   	   	(indent d; printPink "function "; dolist d f x; printnewln "")
										     end
		 (*
		  | printdec (A.PrimitiveDec{name, params, typ}, d)  =    let fun print_tyfield(A.typeField({name,typ}),d) = (printtostream name ; printtostream " : " ; printGreen typ) 
																  in (indent d; printCyan "primitive "; printtostream name ; printtostream "(";
																  dolist 0 print_tyfield params ; printtostream ")";
																  case typ of NONE => printtostream ""
																			 | SOME(s) => (printtostream " : " ; printGreen s)) 
											 			          end
		*)											 			          
	(* function to handle printing of arguments of functions *)                 			          
	(*and printArgs (a::b::c,d) = (PrintExprPretty(a,d) ; printtostream "," ; printArgs(b::c,d))
		| printArgs (a::[],d) = (PrintExprPretty(a,d)) 					    
        | printArgs ([],d) = ()
	*)
    (* function to handle pretty printing of expressions *)
    and PrintExprPretty (A.NilExpr,d) = (indent d ; printRed "nil")
		 |  PrintExprPretty (A.VarExpr(var), d) = (indent d ; lvalue(var, d))
		 |  PrintExprPretty (A.IntegerExpr(I),d) = (indent d ; printtostream (Int.toString(I)))
		 |  PrintExprPretty (A.StringExpr(st),d) = (indent d ; printRed "\"" ; printtostream st ; printRed "\"" )

		 |  PrintExprPretty (A.CallFunctionExpr{func,args},d) = (indent d ;  printBlue func ; printtostream "(" ; docall 0 PrintExprPretty args ; printtostream ")")

		 |  PrintExprPretty (A.OperatorExpr{left,oper,right},d) = (indent 0;  PrintExprPretty(left,0)  ; printtostream " " ; printtostream (opname oper) ; printtostream " " ; PrintExprPretty(right,0))
		 
		 | PrintExprPretty (A.SeqExpr l, d) = (doseq d PrintExprPretty l)

		 | PrintExprPretty (A.AssignExpr{var,exp},d) = (indent d ; lvalue(var,0) ; printtostream " := " ; PrintExprPretty(exp,0))
		 
		 | PrintExprPretty (A.NegativeOpExpr e, d) = ( indent d ; printtostream "-" ; PrintExprPretty(e,d))
	(*	 | PrintExprPretty (A.BracketOpExpr(exps),d) = (indent d ; printnewln "(" ; PrintExprPretty(A.Exp(exp),d + 1) ; printnewln ")")
*)
		 | PrintExprPretty (A.IfExpr{test,then', else'},d) = (printnewln ""; indent d; printRed "if " ; printtostream "(" ; PrintExprPretty(test,0) ; printtostream ")" ;
		 																	printRed " then " ; printnewln "" ; PrintExprPretty(then',d+1);
		 																	case else' of NONE => ()
		 																				| SOME e => (printnewln "" ; indent d ; printRed "else" ; printnewln "" ; PrintExprPretty(e, d+1))
		 																	)   
		 | PrintExprPretty (A.WhileExpr{test,body},d) = (indent d; printRed "while " ; printtostream "(" ; 	PrintExprPretty(test,0) ; printtostream ")" ; printRed " do \n"; PrintExprPretty(body,d+1))
		 
		 | PrintExprPretty (A.ForExpr{var,lo,hi,body},d) =  (indent d; printRed "for " ; printtostream "(" ; printtostream var ; printtostream " := " ;PrintExprPretty(lo,0)
										; printRed " to " ; PrintExprPretty(hi,0) ; printtostream ")" ; printRed " do \n" ; PrintExprPretty (body,d+1))
										
		 | PrintExprPretty (A.BreakExpr,d) = (indent d; printRed "break")
		 
		 | PrintExprPretty (A.LetExpr{decs,body},d) = (indent d ; printRed "let " ; printnewln "" ; dodecs (d+1) printdec decs ; printnewln "" ; 
								  						indent d ; printRed "in" ; printnewln "" ; PrintExprPretty(body, d+1); printnewln ""; 
								 						indent d ; printRed"end \n")
		
		 | PrintExprPretty ((A.ArrayExpr{typ,size,init},d)) = (indent d; printYellow typ; printtostream "[" ; PrintExprPretty(size,0) ; printtostream "] "
								; printYellow "of " ; PrintExprPretty(init,0))
		 | PrintExprPretty (A.RecordExpr{fields, typ}, d) = let
		 														fun f((name,e),d) = (printtostream(name); printtostream "="; PrintExprPretty(e,d); printtostream "")
		 													in
		 														(indent d; printBlue(typ); printtostream "{ " ; dolist d f fields; printtostream " }")
		 													end					
				

	(* function to handle exps and decs lists *)
	(*and PrintProgramPretty (A.Exp(A.Exps(a::b::c)),d) = (PrintExprPretty(a,d) ; printnewln ";" ; PrintProgramPretty(A.Exp(A.Exps(b::c)),d))
		  | PrintProgramPretty (A.Dec(A.Decs(a::b::c)),d) = (printdec(a,d) ; printnewln "" ; PrintProgramPretty(A.Dec(A.Decs(b::c)),d))		
		  | PrintProgramPretty (A.Exp(A.Exps(a::[])),d) = (PrintExprPretty(a,d) ; printnewln "")
		  | PrintProgramPretty (A.Dec(A.Decs(a::[])),d) = (printdec(a,d) ; printnewln "")						      
		  | PrintProgramPretty (_,d) = ()
	*)
in
	(* main handler function *)
    PrintExprPretty (prog,0); printnewln ""; TextIO.flushOut outstream  
end
end
