PROGRAM_NAME = tc
TIGER = $(CURDIR)/tiger
TEST = $(CURDIR)/tests
 

COMMON=${TIGER}/ast.sml

%.lex.sml: %.lex
	mllex $<

%.grm.sml: %.grm
	mlyacc $<

all: tiger

.PHONY: all clean test

clean:
	rm -f ${TIGER}/*.lex.sml ${TIGER}/*.grm.sml ${TIGER}/*.grm.desc ${TIGER}/*.grm.sig
	rm -f tc

tiger: tc.sml tc.mlb ${TIGER}/tiger.grm ${TIGER}/tiger.grm.sml ${TIGER}/tiger.lex ${TIGER}/tiger.lex.sml ${COMMON}
	mlton tc.mlb


